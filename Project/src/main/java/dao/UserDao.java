package dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import model.User;

/**
 * userテーブル用のDao
 *
 * 
 */
public class UserDao {


  // ログインIDとパスワードに紐づくユーザ情報を返す
  public User findByLoginInfo(String loginId, String password) {
    Connection conn = null;

    try {
      // データベースへ接続
      conn = DBManager.getConnection();

      // SELECT文を準備
      String sql = "SELECT * FROM user WHERE login_id=? AND password=?";

      // SELECT文を実行し、結果表を取得
      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setString(1, loginId);
      pStmt.setString(2, password);
      ResultSet rs = pStmt.executeQuery();

      if (!rs.next()) {
        return null;
      }

      // 結果表のレコードを変数に格納
      int id = rs.getInt("id");
      String _loginId = rs.getString("login_id");
      String name = rs.getString("name");
      Date birthDate = rs.getDate("birth_date");
      String _password = rs.getString("password");
      Boolean isAdmin = rs.getBoolean("is_admin");
      Timestamp createDate = rs.getTimestamp("create_date");
      Timestamp updateDate = rs.getTimestamp("update_date");

      return new User(id, _loginId, name, birthDate, _password, isAdmin, createDate, updateDate);
    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }
  }

  public List<User> findAll() {
    Connection conn = null;
    List<User> userList = new ArrayList<User>();

    try {
      // データベースへ接続
      conn = DBManager.getConnection();

      // SELECT文を準備
      String sql = "SELECT * FROM user WHERE is_admin=FALSE";

      // SELECT文を実行し、結果表を取得
      PreparedStatement pStmt = conn.prepareStatement(sql);
      ResultSet rs = pStmt.executeQuery();

      while (rs.next()) {
        // 結果表の各レコードごとに全部のカラムの値を変数に格納
        int id = rs.getInt("id");
        String _loginId = rs.getString("login_id");
        String name = rs.getString("name");
        Date birthDate = rs.getDate("birth_date");
        String _password = rs.getString("password");
        Boolean isAdmin = rs.getBoolean("is_admin");
        Timestamp createDate = rs.getTimestamp("create_date");
        Timestamp updateDate = rs.getTimestamp("update_date");

        // 各レコードごとに全部のカラムの値を持つUserインスタンスを生成
        User user =
            new User(id, _loginId, name, birthDate, _password, isAdmin, createDate, updateDate);

        // リストに各Userインスタンスを追加
        userList.add(user);
      }

      return userList;

    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }
  }

  public void insert(User insertParams) {
    Connection conn = null;

    try {
      // データベースへ接続
      conn = DBManager.getConnection();

      // INSERT文を準備
      String sql =
          "INSERT INTO user(login_id,name,birth_date,password,create_date,update_date) VALUES(?,?,?,?,now(),now())";

      PreparedStatement pStmt = conn.prepareStatement(sql);

      String loginId = insertParams.getLoginId();
      String name = insertParams.getName();
      Date birthDate = insertParams.getBirthDate();
      String password = insertParams.getPassword();

      pStmt.setString(1, loginId);
      pStmt.setString(2, name);
      pStmt.setDate(3, birthDate);
      pStmt.setString(4, password);

      // INSERT文を実行
      pStmt.executeUpdate();

    } catch (SQLException e) {
      e.printStackTrace();
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }
    }
  }

  public User findById(int id) {
    Connection conn = null;

    try {
      // データベースへ接続
      conn = DBManager.getConnection();

      // SELECT文を準備
      String sql = "SELECT * FROM user WHERE id=?";

      // SELECT文を実行し、結果表を取得
      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setInt(1, id);
      ResultSet rs = pStmt.executeQuery();

      if (!rs.next()) {
        return null;
      }

      // 結果表のレコードを変数に格納
      int _id = rs.getInt("id");
      String loginId = rs.getString("login_id");
      String name = rs.getString("name");
      Date birthDate = rs.getDate("birth_date");
      String password = rs.getString("password");
      Boolean isAdmin = rs.getBoolean("is_admin");
      Timestamp createDate = rs.getTimestamp("create_date");
      Timestamp updateDate = rs.getTimestamp("update_date");

      return new User(_id, loginId, name, birthDate, password, isAdmin, createDate, updateDate);
    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }
  }

  // パスワードとパスワード（確認）が二つとも入力されていない場合のupdate
  public void update(int id, String name, Date birthDate) {
    Connection conn = null;

    try {
      // データベースへ接続
      conn = DBManager.getConnection();

      // UPDATE文を準備
      String sql = "UPDATE user SET name=?,birth_date=?,update_date=now() WHERE id=?";

      PreparedStatement pStmt = conn.prepareStatement(sql);

      pStmt.setString(1, name);
      pStmt.setDate(2, birthDate);
      pStmt.setInt(3, id);

      // UPDATE文を実行
      pStmt.executeUpdate();

    } catch (SQLException e) {
      e.printStackTrace();
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }
    }
  }

  // パスワードとパスワード（確認）の入力内容が同じ場合のupdate
  public void update(int id, String name, Date birthDate, String password) {
    Connection conn = null;

    try {
      // データベースへ接続
      conn = DBManager.getConnection();

      // UPDATE文を準備
      String sql = "UPDATE user SET name=?,birth_date=?,password=?,update_date=now() WHERE id=?";

      PreparedStatement pStmt = conn.prepareStatement(sql);

      pStmt.setString(1, name);
      pStmt.setDate(2, birthDate);
      pStmt.setString(3, password);
      pStmt.setInt(4, id);

      // UPDATE文を実行
      pStmt.executeUpdate();

    } catch (SQLException e) {
      e.printStackTrace();
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }
    }
  }

  public void delete(int deleteId) {
    Connection conn = null;

    try {
      // データベースへ接続
      conn = DBManager.getConnection();

      // DELETE文を準備
      String sql = "DELETE FROM user WHERE id=?";

      PreparedStatement pStmt = conn.prepareStatement(sql);

      pStmt.setInt(1, deleteId);

      // DELETE文を実行
      pStmt.executeUpdate();

    } catch (SQLException e) {
      e.printStackTrace();
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }
    }
  }


  // 既に登録されているログインIDかどうかを調べるメソッド。登録されていればtrue、登録されていなければfalseを返す。
  public boolean existsLoginId(String loginId) {
    Connection conn = null;

    // userテーブルに既にログインIDが登録されていればtrue、登録されていなければfalse
    boolean existsLoginId;

    try { // データベースへ接続
      conn = DBManager.getConnection();

      // SELECT文を準備
      String sql = "SELECT * FROM user WHERE login_id=?";

      // SELECT文を実行し、結果表を取得
      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setString(1, loginId);
      ResultSet rs = pStmt.executeQuery();

      // 既にログインIDが登録されている場合
      if (rs.next()) {
        existsLoginId = true;
      }
      // ログインIDが登録されていない場合
      else {
        existsLoginId = false;
      }
      return existsLoginId;

    } catch (SQLException e) {
      e.printStackTrace();
      return true;
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return true;
        }
      }
    }
  }

  public List<User> search(String loginId, String name, String dateStart, String dateEnd) {
    Connection conn = null;
    List<User> userList = new ArrayList<User>();

    try {
      // データベースへ接続
      conn = DBManager.getConnection();

      // SELECT文を準備
      String sql =
          "SELECT * FROM user WHERE is_admin=FALSE";

      // sqlに代入したSELECT文を連結させるためにStringBuilderのインスタンスを宣言
      StringBuilder stringBuilder = new StringBuilder(sql);

      // ログインIDが空欄かどうか
      boolean isLoginIdBlank = loginId.equals("");
      // ユーザ名が空欄かどうか
      boolean isNameBlank = name.equals("");
      // 開始日が空欄かどうか
      boolean isdateStartBlank = dateStart.equals("");
      // 終了日が空欄かどうか
      boolean isdateEndBlank = dateEnd.equals("");

      // SELECT文に連結する文を判定する
      if (!isLoginIdBlank) {
        stringBuilder.append(" AND login_id=?");
      }
      if (!isNameBlank) {
        stringBuilder.append(" AND name LIKE ?");
      }
      if (!isdateStartBlank) {
        stringBuilder.append(" AND birth_date>=?");
      }
      if (!isdateEndBlank) {
        stringBuilder.append(" AND birth_date<=?");
      }

      PreparedStatement pStmt = conn.prepareStatement(stringBuilder.toString());

      // sql文中の?の位置を示すインデックス
      int index = 1;

      // ?に値を設定
      if (!isLoginIdBlank) {
        pStmt.setString(index, loginId);
        index++;
      }
      if (!isNameBlank) {
        pStmt.setString(index, "%" + name + "%");
        index++;
      }
      if (!isdateStartBlank) {
        pStmt.setDate(index, Date.valueOf(dateStart));
        index++;
      }
      if (!isdateEndBlank) {
        pStmt.setDate(index, Date.valueOf(dateEnd));
      }


      // SELECT文を実行し、結果表を取得
      ResultSet rs = pStmt.executeQuery();

      while (rs.next()) {
        // 結果表の各レコードごとに全部のカラムの値を変数に格納
        int id = rs.getInt("id");
        String _loginId = rs.getString("login_id");
        String _name = rs.getString("name");
        Date birthDate = rs.getDate("birth_date");
        String password = rs.getString("password");
        Boolean isAdmin = rs.getBoolean("is_admin");
        Timestamp createDate = rs.getTimestamp("create_date");
        Timestamp updateDate = rs.getTimestamp("update_date");

        // 各レコードごとに全部のカラムの値を持つUserインスタンスを生成
        User user =
            new User(id, _loginId, _name, birthDate, password, isAdmin, createDate, updateDate);

        // リストに各Userインスタンスを追加
        userList.add(user);
      }

      return userList;

    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }
  }

}

