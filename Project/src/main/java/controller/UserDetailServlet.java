package controller;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import dao.UserDao;
import model.User;

/**
 * Servlet implementation class UserDetailServlet
 */
@WebServlet("/UserDetailServlet")
public class UserDetailServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserDetailServlet() {
        super();

    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

      HttpSession session = request.getSession();
      User loginUser = (User) session.getAttribute("loginUser");

      // セッションにログインユーザの情報がない場合、ログイン画面のサーブレットにリダイレクト
      if (loginUser == null) {
        response.sendRedirect("LoginServlet");
        return;
      }


      // リクエストパラメータの文字コードを指定
      request.setCharacterEncoding("UTF-8");

      // リクエストパラメータidを取得
      int id = Integer.parseInt(request.getParameter("id"));

      UserDao userDao = new UserDao();
      // idに紐づくユーザをuserテーブルから検索し、そのユーザの情報を持つUserインスタンスを格納
      User detailUser = userDao.findById(id);

      // リクエストスコープにUserインスタンスをセット
      request.setAttribute("detailUser", detailUser);

      // ユーザ情報詳細参照画面のjspにフォワード
      RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userDetail.jsp");
      dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


	}

}
