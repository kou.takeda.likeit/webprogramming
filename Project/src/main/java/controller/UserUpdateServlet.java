package controller;

import java.io.IOException;
import java.sql.Date;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import dao.UserDao;
import model.User;
import util.PasswordEncorder;

/**
 * Servlet implementation class UserUpdateServlet
 */
@WebServlet("/UserUpdateServlet")
public class UserUpdateServlet extends HttpServlet {
  private static final long serialVersionUID = 1L;

  /**
   * @see HttpServlet#HttpServlet()
   */
  public UserUpdateServlet() {
    super();

  }

  /**
   * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {

    HttpSession session = request.getSession();
    User loginUser = (User) session.getAttribute("loginUser");

    // セッションにログインユーザの情報がない場合、ログイン画面のサーブレットにリダイレクト
    if (loginUser == null) {
      response.sendRedirect("LoginServlet");
      return;
    }


    // リクエストパラメータの文字コードを指定
    request.setCharacterEncoding("UTF-8");

    // リクエストパラメータidを取得
    int id = Integer.parseInt(request.getParameter("id"));

    UserDao userDao = new UserDao();
    // idに紐づくユーザをuserテーブルから検索し、そのユーザの情報を持つUserインスタンスを格納
    User updateUser = userDao.findById(id);

    // セッションスコープにUserインスタンスをセット
    session.setAttribute("updateUser", updateUser);


    // ユーザ情報更新画面のjspにフォワード
    RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp");
    dispatcher.forward(request, response);

  }

  /**
   * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    HttpSession session = request.getSession();

    // リクエストパラメータの文字コードを指定
    request.setCharacterEncoding("UTF-8");

    // リクエストパラメータを取得
    int id = Integer.parseInt(request.getParameter("user-id"));
    String password = request.getParameter("password");
    String passwordConfirm = request.getParameter("password-confirm");
    String name = request.getParameter("user-name");
    String birthDate = request.getParameter("birth-date");

    UserDao userDao = new UserDao();

    // Userインスタンスを取得
    User updateUser = (User) session.getAttribute("updateUser");


    // ユーザ名が空欄かどうか
    boolean isNameBlank = name.equals("");
    // 生年月日が空欄かどうか
    boolean isBirthDateBlank = birthDate.equals("");

    // パスワードとパスワード（確認）の入力内容が異なる場合やパスワード以外に未入力の項目がある場合
    if (!password.equals(passwordConfirm) || isNameBlank || isBirthDateBlank) {
      // リクエストスコープにエラーメッセージをセット
      request.setAttribute("errMsg", "入力された内容は正しくありません");

      // 入力されたユーザ名を画面に表示するため、Userインスタンスが持つユーザ名を更新
      updateUser.setName(name);
      
      // 生年月日が空欄である場合
      if (isBirthDateBlank) {
        // 入力された生年月日（空欄）を画面に表示するため、Userインスタンスが持つ生年月日を更新
        updateUser.setBirthDate(null);
      }
      // 生年月日が空欄でない場合
      else {
        // 入力された生年月日（空欄以外）を画面に表示するため、Userインスタンスが持つ生年月日を更新
        updateUser.setBirthDate(Date.valueOf(birthDate));
      }

      // ユーザ情報更新画面のjspにフォワード
      RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp");
      dispatcher.forward(request, response);
      return;
    }


    // パスワードとパスワード（確認）が二つとも空欄の場合
    if (password.equals("") && passwordConfirm.equals("")) {
      // Userインスタンスとuserテーブルのユーザ名、生年月日を更新
      updateUser.setName(name);
      updateUser.setBirthDate(Date.valueOf(birthDate));
      userDao.update(id, name, Date.valueOf(birthDate));
    }
    // パスワードとパスワード（確認）が二つとも同じ場合
    else {
      // 暗号化されたパスワード
      String encordPassword = PasswordEncorder.encordPassword(password);

      // Userインスタンス、userテーブルのユーザ名、生年月日、パスワードを更新
      updateUser.setName(name);
      updateUser.setBirthDate(Date.valueOf(birthDate));
      updateUser.setPassword(encordPassword);

      userDao.update(id, name, Date.valueOf(birthDate), encordPassword);
    }

    // セッションから更新ユーザの情報を削除
    session.removeAttribute("updateUser");

    // ユーザ一覧のサーブレットにリダイレクト
    response.sendRedirect("UserListServlet");

  }

}
