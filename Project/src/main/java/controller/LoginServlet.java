package controller;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import dao.UserDao;
import model.User;
import util.PasswordEncorder;

/**
 * Servlet implementation class LoginServlet
 */
@WebServlet("/LoginServlet")
public class LoginServlet extends HttpServlet {
  private static final long serialVersionUID = 1L;

  /**
   * @see HttpServlet#HttpServlet()
   */
  public LoginServlet() {
    super();


  }

  /**
   * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {

    HttpSession session = request.getSession();
    User loginUser = (User) session.getAttribute("loginUser");

    // セッションにログインユーザの情報がある場合、ユーザ一覧画面にリダイレクトさせる
    if (loginUser != null) {
      response.sendRedirect("UserListServlet");
      return;
    }

    // ログイン画面のjspへフォワード
    RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userLogin.jsp");
    dispatcher.forward(request, response);
  }

  /**
   * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {

    // リクエストパラメータの文字コードを指定
    request.setCharacterEncoding("UTF-8");

    // リクエストパラメータの入力項目を取得
    String loginId = request.getParameter("user-loginid");
    String password = request.getParameter("password");

    UserDao userDao = new UserDao();

    // 暗号化されたパスワード
    String encordPassword = PasswordEncorder.encordPassword(password);

    // 入力されたログインIDとパスワードを持つユーザがuserテーブルに存在するか検索
    User user = userDao.findByLoginInfo(loginId, encordPassword);

    // 入力されたログインIDとパスワードを持つユーザがuserテーブルに存在しなかった場合
    if (user == null) {
      // リクエストスコープにエラーメッセージをセット
      request.setAttribute("errMsg", "ログインIDまたはパスワードが異なります");
      // 入力されたログインIDを画面に表示するため、リクエストスコープに値をセット
      request.setAttribute("loginId", loginId);

      // ログイン画面のjspにフォワード
      RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userLogin.jsp");
      dispatcher.forward(request, response);
      return;
    }

    /*
     * 入力されたログインIDとパスワードを持つユーザがuserテーブルに存在した場合、 セッションにログインユーザの情報をセット
     */
    HttpSession session = request.getSession();
    session.setAttribute("loginUser", user);

    // ユーザ一覧のサーブレットにリダイレクト
    response.sendRedirect("UserListServlet");

  }
}
