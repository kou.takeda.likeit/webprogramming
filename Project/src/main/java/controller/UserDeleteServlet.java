package controller;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import dao.UserDao;
import model.User;

/**
 * Servlet implementation class UserDeleteServlet
 */
@WebServlet("/UserDeleteServlet")
public class UserDeleteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserDeleteServlet() {
        super();

    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

      HttpSession session = request.getSession();
      User loginUser = (User) session.getAttribute("loginUser");

      // セッションにログインユーザの情報がない場合、ログイン画面のサーブレットにリダイレクト
      if (loginUser == null) {
        response.sendRedirect("LoginServlet");
        return;
      }

      // リクエストパラメータの文字コードを指定
      request.setCharacterEncoding("UTF-8");

      // ユーザ削除画面のjspで押したボタンを格納する
      String answer = request.getParameter("answer");

      // ユーザ一覧のjspで削除ボタンを押したときの処理
      if (answer == null) {
        // リクエストパラメータを取得
        int deleteId = Integer.parseInt(request.getParameter("id"));
        String deleteLoginId = request.getParameter("loginId");

        // リクエストスコープにUserインスタンスをセット
        request.setAttribute("deleteId", deleteId);
        request.setAttribute("deleteLoginId", deleteLoginId);

        // ユーザ削除画面のjspにフォワード
        RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userDelete.jsp");
        dispatcher.forward(request, response);
      }
      // ユーザ削除画面のjspで「はい」ボタンを押したときの処理
      else if (answer.equals("yes")) {
        // リクエストパラメータdeleteIdを取得
        int deleteId = Integer.parseInt(request.getParameter("deleteId"));

        UserDao userDao = new UserDao();
        // DELETE文を実行するメソッドの呼び出し
        userDao.delete(deleteId);

        // ユーザ一覧のサーブレットにリダイレクト
        response.sendRedirect("UserListServlet");
      }
      // ユーザ削除画面のjspで「いいえ」ボタンを押したときの処理
      else if (answer.equals("no")) {
        // ユーザ一覧のサーブレットにリダイレクト
        response.sendRedirect("UserListServlet");
      }

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	}

}
