package controller;

import java.io.IOException;
import java.sql.Date;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import dao.UserDao;
import model.User;
import util.PasswordEncorder;

/**
 * Servlet implementation class UserAddServlet
 */
@WebServlet("/UserAddServlet")
public class UserAddServlet extends HttpServlet {
  private static final long serialVersionUID = 1L;

  /**
   * @see HttpServlet#HttpServlet()
   */
  public UserAddServlet() {
    super();

  }

  /**
   * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {

    HttpSession session = request.getSession();
    User loginUser = (User) session.getAttribute("loginUser");

    // セッションにログインユーザの情報がない場合、ログイン画面のサーブレットにリダイレクト
    if (loginUser == null) {
      response.sendRedirect("LoginServlet");
      return;
    }

    // ユーザ新規登録のjspにフォワード
    RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userAdd.jsp");
    dispatcher.forward(request, response);

  }

  /**
   * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {

    // リクエストパラメータの文字コードを指定
    request.setCharacterEncoding("UTF-8");

    // リクエストパラメータの入力項目を取得
    String loginId = request.getParameter("user-loginid");
    String password = request.getParameter("password");
    String passwordConfirm = request.getParameter("password-confirm");
    String name = request.getParameter("user-name");
    String birthDate = request.getParameter("birth-date");


    UserDao userDao = new UserDao();

    // userテーブルに既に登録されているログインIDかどうか
    boolean existsLoginId = userDao.existsLoginId(loginId);

    // パスワードとパスワード（確認）の入力内容が異なっているかどうか
    boolean isDifferentPasswordValues = !password.equals(passwordConfirm);

    // ログインIDが空欄かどうか
    boolean isLoginIdBlank = loginId.equals("");
    // パスワードが空欄かどうか
    boolean isPasswordBlank = password.equals("");
    // パスワード(確認)が空欄かどうか
    boolean isPasswordConfirmBlank = passwordConfirm.equals("");
    // ユーザ名が空欄かどうか
    boolean isNameBlank = name.equals("");
    // 生年月日が空欄かどうか
    boolean isBirthDateBlank = birthDate.equals("");

    // 入力項目が一つ以上空欄かどうか
    boolean isBlank = isLoginIdBlank || isPasswordBlank || isPasswordConfirmBlank || isNameBlank
        || isBirthDateBlank;


    /*
     * 「既に登録されているログインIDが入力された」または「パスワードとパスワード（確認）の入力内容が異なっている」 または「入力項目に一つでも空欄がある」場合
     */
    if (existsLoginId || isDifferentPasswordValues || isBlank) {
      // リクエストスコープにエラーメッセージをセット
      request.setAttribute("errMsg", "入力された内容は正しくありません");

      // ログインIDが空欄でない場合
      if (isLoginIdBlank == false) {
        // 入力されたログインIDを画面に表示するため、リクエストスコープに値をセット
        request.setAttribute("loginId", loginId);
      }
      // ユーザ名が空欄でない場合
      if (isNameBlank == false) {
        // 入力されたユーザ名を画面に表示するため、リクエストスコープに値をセット
        request.setAttribute("name", name);
      }
      // 生年月日が空欄でない場合
      if (isBirthDateBlank == false) {
        // 入力された生年月日を画面に表示するため、リクエストスコープに値をセット
        request.setAttribute("birthDate", birthDate);
      }


      // ユーザ新規登録画面のjspにフォワード
      RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userAdd.jsp");
      dispatcher.forward(request, response);
      return;
    }

    // 暗号化されたパスワード
    String encordPassword = PasswordEncorder.encordPassword(password);

    // INSERT文を実行するメソッドの呼び出し
    userDao.insert(new User(loginId, name, Date.valueOf(birthDate), encordPassword));

    // ユーザ一覧のサーブレットにリダイレクト
    response.sendRedirect("UserListServlet");

  }

}
