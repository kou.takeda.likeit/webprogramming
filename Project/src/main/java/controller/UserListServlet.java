package controller;

import java.io.IOException;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import dao.UserDao;
import model.User;

/**
 * Servlet implementation class UserListServlet
 */
@WebServlet("/UserListServlet")
public class UserListServlet extends HttpServlet {
  private static final long serialVersionUID = 1L;

  /**
   * @see HttpServlet#HttpServlet()
   */
  public UserListServlet() {
    super();

  }

  /**
   * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {


    HttpSession session = request.getSession();
    User loginUser = (User) session.getAttribute("loginUser");

    // セッションにログインユーザの情報がない場合、ログイン画面のサーブレットにリダイレクト
    if (loginUser == null) {
      response.sendRedirect("LoginServlet");
      return;
    }


    // ユーザ一覧情報を取得
    UserDao userDao = new UserDao();
    List<User> userList = userDao.findAll();

    // リクエストスコープにユーザ一覧情報をセット
    request.setAttribute("userList", userList);

    // ユーザ一覧のjspにフォワード
    RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userList.jsp");
    dispatcher.forward(request, response);
  }

  /**
   * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {

    // リクエストパラメータの文字コードを指定
    request.setCharacterEncoding("UTF-8");

    // リクエストパラメータの入力項目を取得
    String loginId = request.getParameter("user-loginid");
    String name = request.getParameter("user-name");
    String dateStart = request.getParameter("date-start");
    String dateEnd = request.getParameter("date-end");

    // 検索結果を取得
    UserDao userDao = new UserDao();
    List<User> userList =
        userDao.search(loginId, name, dateStart, dateEnd);

    // リクエストスコープに検索結果をセット
    request.setAttribute("userList", userList);

    // 入力されたログインIDをテキストボックスに残すため、リクエストスコープに値をセット
    request.setAttribute("loginId", loginId);

    // 入力されたユーザ名をテキストボックスに残すため、リクエストスコープに値をセット
    request.setAttribute("name", name);

    // 入力された生年月日をテキストボックスに残すため、リクエストスコープに値をセット
    request.setAttribute("dateStart", dateStart);
    request.setAttribute("dateEnd", dateEnd);



    // ユーザ一覧のjspにフォワード
    RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userList.jsp");
    dispatcher.forward(request, response);

  }

}
