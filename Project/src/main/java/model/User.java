package model;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Timestamp;

/**
 * Userテーブルのデータを格納するためのBeans
 */

public class User implements Serializable {
  private int id;
  private String loginId;
  private String name;
  private Date birthDate;
  private String password;
  private boolean isAdmin;
  private Timestamp createDate;
  private Timestamp updateDate;

  public User() {

  }

  public User(int id, String loginId, String name, Date birthDate, String password, Boolean isAdmin,
      Timestamp createDate, Timestamp updateDate) {
    super();
    this.id = id;
    this.loginId = loginId;
    this.name = name;
    this.birthDate = birthDate;
    this.password = password;
    this.isAdmin = isAdmin;
    this.createDate = createDate;
    this.updateDate = updateDate;
  }

  public User(String loginId, String name, Date birthDate, String password) {
    this.loginId = loginId;
    this.name = name;
    this.birthDate = birthDate;
    this.password = password;
  }

  public User(int id, String name, Date birthDate, String password) {
    this.id = id;
    this.name = name;
    this.birthDate = birthDate;
    this.password = password;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }


  public String getLoginId() {
    return loginId;
  }

  public void setLoginId(String loginId) {
    this.loginId = loginId;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Date getBirthDate() {
    return birthDate;
  }

  public void setBirthDate(Date birthDate) {
    this.birthDate = birthDate;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String _password) {
    this.password = _password;
  }


  public boolean isAdmin() {
    return isAdmin;
  }

  public void setAdmin(boolean isAdmin) {
    this.isAdmin = isAdmin;
  }


  public Timestamp getCreateDate() {
    return createDate;
  }

  public void setCreateDate(Timestamp createDate) {
    this.createDate = createDate;
  }

  public Timestamp getUpdateDate() {
    return updateDate;
  }

  public void setUpdateDate(Timestamp updateDate) {
    this.updateDate = updateDate;
  }

}
